package br.edu.ufabc.games.racing;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;

import br.edu.ufabc.games.racing.screen.AbstractScreen;
import br.edu.ufabc.games.racing.screen.CarSelectionScreen;
import br.edu.ufabc.games.racing.screen.CreditsScreen;
import br.edu.ufabc.games.racing.screen.GameScreen;
import br.edu.ufabc.games.racing.screen.StartScreen;
import br.edu.ufabc.games.racing.util.Commands;
import br.edu.ufabc.games.racing.util.Utils;

public class RacingGame extends Game implements InputProcessor, ControllerListener {

	AbstractScreen currentScreen;
	public static AssetManager assetManager;
//	public GamePad gamepad = new GamePad();
	public void create() {
		
		Gdx.input.setInputProcessor(this);
		Controllers.addListener(this);
		for (Controller c : Controllers.getControllers()) {
			System.out.println("Detected - " + c.getName());
		}

		assetManager = new AssetManager();
		//assetManager.load("batmovel.g3db", Model.class);
		assetManager.load("race_track.g3db", Model.class);
		assetManager.load("Car.g3db", Model.class);
		assetManager.load("track_marker1.g3db", Model.class);
		assetManager.load("carros/black_car.g3db", Model.class);
		assetManager.load("carros/yellow_car.g3db", Model.class);
		assetManager.load("carros/red_car.g3db", Model.class);
		assetManager.load("carros/brown_car.g3db", Model.class);
		
		currentScreen = new StartScreen("START");
		setScreen(currentScreen);
	}

	public void render() {
		currentScreen.render(Gdx.graphics.getDeltaTime());
		// logica minima do controller
		// quando uma tela termina sua fun��o, passa para a pr�xima
		if (currentScreen.isDone()) {
			if (currentScreen.getId().equals("START")) {
				currentScreen = new CarSelectionScreen("CAR");
			} else if (currentScreen.getId().equals("CAR")) {
				currentScreen = new GameScreen("GAME");
			} else if (currentScreen.getId().equals("GAME")) {
					currentScreen = new CreditsScreen("CREDITS");
			} else {
				currentScreen = new StartScreen("START");
			}
		}
	}

	/*--- M�todos do InputProcessor - (touch + teclado)--------------------------------------*/
	@Override
	public boolean keyDown(int keycode) {

		// TODO Auto-generated method stub
		if (keycode == Input.Keys.UP) {
			Commands.comandos[Commands.FRENTE] = true;
			Commands.valores[Commands.FRENTE] = 1;
			return true;
		}
		if (keycode == Input.Keys.DOWN) {
			Commands.comandos[Commands.TRAS] = true;
			Commands.valores[Commands.TRAS] = 1;
			return true;
		}
		if (keycode == Input.Keys.LEFT) {
			Commands.comandos[Commands.ESQUERDA] = true;
			Commands.valores[Commands.ESQUERDA] = 1;
			return true;
		}
		if (keycode == Input.Keys.RIGHT) {
			Commands.comandos[Commands.DIREITA] = true;
			Commands.valores[Commands.DIREITA] = 1;
			return true;
		}
		if (keycode == Input.Keys.W) {
			Commands.comandos[Commands.FRENTE] = true;
			Commands.valores[Commands.FRENTE] = 1;
			return true;
		}
		if (keycode == Input.Keys.S) {
			Commands.comandos[Commands.TRAS] = true;
			Commands.valores[Commands.TRAS] = 1;
			return true;
		}
		if (keycode == Input.Keys.A) {
			Commands.comandos[Commands.ESQUERDA] = true;
			Commands.valores[Commands.ESQUERDA] = 1;
			return true;
		}
		if (keycode == Input.Keys.D) {
			Commands.comandos[Commands.DIREITA] = true;
			Commands.valores[Commands.DIREITA] = 1;
			return true;
		}
		if (keycode == Input.Keys.ENTER) {
			Commands.comandos[Commands.GO] = true;
			Commands.valores[Commands.GO] = 1;
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		if (keycode == Input.Keys.UP) {
			Commands.comandos[Commands.FRENTE] = false;
			Commands.valores[Commands.FRENTE] = 0;
			Commands.comandos[Commands.SETACIMA] = true;
			Commands.valores[Commands.SETACIMA] = 1;
			return true;
		}
		if (keycode == Input.Keys.DOWN) {
			Commands.comandos[Commands.TRAS] = false;
			Commands.valores[Commands.TRAS] = 0;
			Commands.comandos[Commands.SETABAIXO] = true;
			Commands.valores[Commands.SETABAIXO] = 1;
			return true;
		}
		if (keycode == Input.Keys.LEFT) {
			Commands.comandos[Commands.ESQUERDA] = false;
			Commands.valores[Commands.ESQUERDA] = 0;
			Commands.comandos[Commands.SETAESQUERDA] = true;
			Commands.valores[Commands.SETAESQUERDA] = 1;
			return true;
		}
		if (keycode == Input.Keys.RIGHT) {
			Commands.comandos[Commands.DIREITA] = false;
			Commands.valores[Commands.DIREITA] = 0;
			Commands.comandos[Commands.SETADIREITA] = true;
			Commands.valores[Commands.SETADIREITA] = 1;
			return true;
		}
		if (keycode == Input.Keys.W) {
			Commands.comandos[Commands.FRENTE] = false;
			Commands.valores[Commands.FRENTE] = 0;
			return true;
		}
		if (keycode == Input.Keys.S) {
			Commands.comandos[Commands.TRAS] = false;
			Commands.valores[Commands.TRAS] = 0;
			return true;
		}
		if (keycode == Input.Keys.A) {
			Commands.comandos[Commands.ESQUERDA] = false;
			Commands.valores[Commands.ESQUERDA] = 0;
			Commands.comandos[Commands.SETAESQUERDA] = true;
			Commands.valores[Commands.SETAESQUERDA] = 1;
			return true;
		}
		if (keycode == Input.Keys.D) {
			Commands.comandos[Commands.DIREITA] = false;
			Commands.valores[Commands.DIREITA] = 0;
			Commands.comandos[Commands.SETADIREITA] = true;
			Commands.valores[Commands.SETADIREITA] = 1;
			return true;
		}
		if (keycode == Input.Keys.ENTER) {
			Commands.comandos[Commands.GO] = false;
			Commands.valores[Commands.GO] = 0;
			return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	/*--- M�todos do Controller - (touch + teclado)--------------------------------------*/

	@Override
	public void connected(Controller controller) {
		// TODO Auto-generated method stub

	}

	@Override
	public void disconnected(Controller controller) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		System.out.println(buttonCode);
		if(controller.getName().equals("Twin USB Joystick") && buttonCode == 2) {
			Commands.comandos[Commands.FRENTE] = true;
			Commands.valores[Commands.FRENTE] = 1;
			return true;
		}
		if(controller.getName().equals("Twin USB Joystick") && buttonCode == 3) {
			Commands.comandos[Commands.TRAS] = true;
			Commands.valores[Commands.TRAS] = 1;
			return true;
		}
		if (buttonCode == 1) {
			Commands.comandos[Commands.GO] = true;
			Commands.valores[Commands.GO] = 1;
			return true;
		}
		return false;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		// TODO Auto-generated method stub
		
		if(controller.getName().equals("Twin USB Joystick") && buttonCode == 2) {
			Commands.comandos[Commands.FRENTE] = false;
			Commands.valores[Commands.FRENTE] = 0;
			return true;
		}
		if(controller.getName().equals("Twin USB Joystick") && buttonCode == 3) {
			Commands.comandos[Commands.TRAS] = false;
			Commands.valores[Commands.TRAS] = 0;
			return true;
		}
		
		if (buttonCode == 1) {
			Commands.comandos[Commands.GO] = false;
			Commands.valores[Commands.GO] = 0;
			return true;
		}
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		// TODO Auto-generated method stub
		if (axisCode == 3 || (controller.getName().equals("Twin USB Joystick") && axisCode==1)) {
			if (value >= 0.2f) {
				Commands.comandos[Commands.DIREITA] = true;
				Commands.comandos[Commands.ESQUERDA] = false;
				Commands.valores[Commands.DIREITA] = value;
				return true;
			} else if (value <= -0.2f) {
				Commands.comandos[Commands.ESQUERDA] = true;
				Commands.comandos[Commands.DIREITA] = false;
				Commands.valores[Commands.ESQUERDA] = -value;
				return true;
			} else {
				Commands.comandos[Commands.DIREITA] = false;
				Commands.comandos[Commands.ESQUERDA] = false;
				Commands.valores[Commands.DIREITA] = 0;
				Commands.valores[Commands.ESQUERDA] = 0;
				return true;
			}
		} else if (axisCode == 1) {
			if (value >= 0.3f) {
				Commands.comandos[Commands.CAMERADIREITA] = true;
				Commands.comandos[Commands.CAMERAESQUERDA] = false;
				Commands.valores[Commands.CAMERADIREITA] = value;
				return true;
			} else if (value <= -0.3f) {
				Commands.comandos[Commands.CAMERAESQUERDA] = true;
				Commands.comandos[Commands.CAMERADIREITA] = false;
				Commands.valores[Commands.CAMERAESQUERDA] = -value;
				return true;
			} else {
				Commands.comandos[Commands.CAMERADIREITA] = false;
				Commands.comandos[Commands.CAMERAESQUERDA] = false;
				Commands.valores[Commands.CAMERADIREITA] = 0;
				Commands.valores[Commands.CAMERAESQUERDA] = 0;
				return true;
			}
		} else if (axisCode == 4) {
			if (value >= 0.3f) {
				Commands.comandos[Commands.FRENTE] = true;
				Commands.valores[Commands.FRENTE] = value;
				return true;
			} else {
				Commands.comandos[Commands.FRENTE] = false;
				Commands.valores[Commands.FRENTE] = 0;
				return true;
			}
		} else if (axisCode == 5) {
			if (value >= 0.3f) {
				Commands.comandos[Commands.TRAS] = true;
				Commands.valores[Commands.TRAS] = value;
				return true;
			} else {
				Commands.comandos[Commands.TRAS] = false;
				Commands.valores[Commands.TRAS] = 0;
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection value) {
		System.out.println("Pov: "+povCode);
		if(value == PovDirection.north) {
			Commands.comandos[Commands.SETACIMA] = true;
			Commands.valores[Commands.SETACIMA] = 1;
		}
		if(value == PovDirection.south) {
			Commands.comandos[Commands.SETABAIXO] = true;
			Commands.valores[Commands.SETABAIXO] = 1;
		}
		if(value == PovDirection.east) {
			Commands.comandos[Commands.SETADIREITA] = true;
			Commands.valores[Commands.SETADIREITA] = 1;
		}
		if(value == PovDirection.west) {
			Commands.comandos[Commands.SETAESQUERDA] = true;
			Commands.valores[Commands.SETAESQUERDA] = 1;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		// TODO Auto-generated method stub
		return false;
	}
}
