package br.edu.ufabc.games.racing.model;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector3;

import br.edu.ufabc.games.racing.util.Commands;

public class Car {
	private GameObject estados;
	private int estado;
	public Vector3 velocidade;
	private float camdiff = 0f;
	private float angulo = 0f;
	public static final int IDLE = 0;
	public static final int RUN = 1;
	public static final int SHOOT = 2;
	public static final int DYING = 3;
	public static final int DEAD = 4;
	
	private static float ACELERACAO = 2f;
	private static final float FRENAGEM = 3f;
	private static final float ACELERACAORE = 1f;
	private static final float DESACELERACAO = 0.5f;
	private static final float VOLANTE = 50f;
	private static final float CAMERA = 3f;

	private static final float LIMVEL = 5f;
	private static final float LIMVOL = 2f;
	private static Sound carSound;
	private static long carSoundId;

	public Car() {
		estados = new GameObject(ModelFactory.getModelbyName("brown_car"));
		estados.transform.scale(1.3f, 1.3f, 1.3f);
		estados.transform.translate(-340f, -2.8f, -400f);    // ponto de inicio da corrida 
		estados.transform.rotate(Vector3.Y, 45);
		estados.setAngle(45);
		velocidade = new Vector3(0,0,0);
		carSound = Gdx.audio.newSound(Gdx.files.internal("engine-loop-1-normalized.wav"));
		carSoundId = carSound.play();
		carSound.setVolume(carSoundId, 0.0f);
		carSound.setLooping(carSoundId, true);		
	}
	
	public Car(String carModel) {
		estados = new GameObject(ModelFactory.getModelbyName(carModel));
		estados.transform.scale(1.3f, 1.3f, 1.3f);
		estados.transform.translate(-340f, -2.8f, -400f);    // ponto de inicio da corrida 
		estados.transform.rotate(Vector3.Y, 45);
		estados.setAngle(45);
		velocidade = new Vector3(0,0,0);
		carSound = Gdx.audio.newSound(Gdx.files.internal("engine-loop-1-normalized.wav"));
		carSoundId = carSound.play();
		carSound.setVolume(carSoundId, 0.0f);
		carSound.setLooping(carSoundId, true);		
	}
	
	public void dispose() {
		carSound.stop();
		carSound.dispose();
	}

	public void update(float delta) {
		estados.update(delta);
		if (estado == SHOOT && estados.isDone()) {
			estados.reset();
			estado = IDLE;
		} else if (estado == DYING && estados.isDone()) {
			estados.reset();
			carSound.stop();
			estado = DEAD;
		}
		carSound.setVolume(carSoundId, Math.abs(1.0f*velocidade.z/(LIMVEL*1.0f)));
		carSound.setPitch(carSoundId, 1.8f*Math.abs(1.0f*velocidade.z/(LIMVEL*1.0f)));
	}

	public void parar(float delta, float value) {
		if (velocidade.z <= -1f) 
			velocidade.z = -1;
		else {
			if(velocidade.z > 0) {
				velocidade.z -= FRENAGEM * delta * value;
			}
			else {
				velocidade.z -= ACELERACAORE * delta * value;
			}
			estados.setAngle(camdiff*delta*CAMERA);
			camdiff -= camdiff*delta*CAMERA;
		}
			
		estados.transform.translate(velocidade);
	}

	public void andar(float delta, float value) {
		if (velocidade.z >= LIMVEL)
			velocidade.z = LIMVEL;
		else {
			velocidade.z += ACELERACAO * delta * value ;
			estados.setAngle(camdiff*delta*CAMERA);
			camdiff -= camdiff*delta*CAMERA;
		}
		estados.transform.translate(velocidade);
	}
	public void desacelerar(float delta) {
		velocidade.z /= 1f+DESACELERACAO*delta ;
		estados.transform.translate(velocidade);
	}

	public void virarEsquerda(float delta, float value) {
		//angulo+=delta*VOLANTE*value;
		angulo=delta*VOLANTE*value;
		if(angulo >= LIMVOL*velocidade.z) angulo = LIMVOL*velocidade.z;
		estados.transform.rotate(Vector3.Y, angulo);
		estados.setAngle(angulo);
	}

	public void virarDireita(float delta, float value) {
//		angulo-=delta*VOLANTE*value;
		angulo=-delta*VOLANTE*value;
		if(angulo <= -LIMVOL*velocidade.z) angulo = -LIMVOL*velocidade.z;
		estados.transform.rotate(Vector3.Y, angulo);
		estados.setAngle(angulo);
	}
	public void virarCameraEsquerda(float delta, float value) {
		estados.setAngle(CAMERA*value);
		camdiff -= CAMERA*value;
	}

	public void virarCameraDireita(float delta, float value) {
		estados.setAngle(-CAMERA*value);
		camdiff += CAMERA*value;
	}
	public void pararDeVirar(float delta) {
		angulo /= 2f;
		estados.transform.rotate(Vector3.Y, angulo);
		estados.setAngle(angulo);
	}

	public void atirar() {
		estado = SHOOT;
	}

	public void morrer() {
		dispose();
		estado = DYING;
	}

	public int getEstado() {
		return estado;
	}

	public GameObject getCurrent() {
		return estados;
	}
	
	public void desacelerar() {
		ACELERACAO = -2f;
	}
	public void acelerar() {
		ACELERACAO = 2f;
	}
}
