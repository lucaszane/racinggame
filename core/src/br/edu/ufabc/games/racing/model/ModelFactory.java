package br.edu.ufabc.games.racing.model;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g3d.Model;

import br.edu.ufabc.games.racing.RacingGame;

public class ModelFactory {
	
	private static HashMap<String, Model> modelos = new HashMap<String,Model>();
	
	static {
		//ModelLoader<ModelParameters> loader;
		//loader = new G3dModelLoader(new UBJsonReader());
		System.out.println("Carregando modelos...");
	     //modelos.put("BATMOVEL", (Model)RacingGame.assetManager.get("batmovel.g3db"));
	     modelos.put("CHAO", (Model)RacingGame.assetManager.get("race_track.g3db"));
	     modelos.put("car1", (Model)RacingGame.assetManager.get("Car.g3db"));
	     modelos.put("track", (Model)RacingGame.assetManager.get("track_marker1.g3db"));
	     modelos.put("black_car", (Model)RacingGame.assetManager.get("carros/black_car.g3db"));
	     modelos.put("yellow_car", (Model)RacingGame.assetManager.get("carros/yellow_car.g3db"));
	     modelos.put("red_car", (Model)RacingGame.assetManager.get("carros/red_car.g3db"));
	     modelos.put("brown_car", (Model)RacingGame.assetManager.get("carros/brown_car.g3db"));
	     
	     System.out.println("Modelos carregados!");
	}
	
	public static Model getModelbyName(String name) {
		return modelos.get(name);
	}

}
