package br.edu.ufabc.games.racing.model;

public class Pedra {
	private GameObject obj;
	
    public Pedra() {
    	obj = new GameObject(ModelFactory.getModelbyName("PEDRA"));
    }
    
    public void update(float delta) {
       obj.update(delta);	
    }
    
    public GameObject getCurrent() {
    	return obj;
    }
}
