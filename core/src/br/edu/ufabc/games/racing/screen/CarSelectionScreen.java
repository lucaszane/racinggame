package br.edu.ufabc.games.racing.screen;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

import br.edu.ufabc.games.racing.RacingGame;
import br.edu.ufabc.games.racing.model.Car;
import br.edu.ufabc.games.racing.util.ChasingCamera;
import br.edu.ufabc.games.racing.util.Commands;
import br.edu.ufabc.games.racing.util.Parameters;

public class CarSelectionScreen extends AbstractScreen {
	private Texture texture;
	private Texture arrowLeft;
	private Texture arrowRight;
	private Texture arrowUp;
	private Texture arrowDown;
	private int currentSprite;
	private final int SPRITE_WIDTH = 170;
	private final int SPRITE_HEIGHT = 155;
	private SpriteBatch spriteBatch;
	private Matrix4 viewMatrix;
	private Matrix4 tranMatrix;
	private BitmapFont font;
	private BitmapFont fontImprint;
	private BitmapFont fontImprintWhite;
	private boolean loaded = false;
	private int progress;
	private float time=0;
	private boolean visible=false;
	private Music   music;
	public static int numberLaps;
	public static int selectedCar;
	public static final String carOptions[] = {"black_car", "yellow_car", "red_car", "brown_car"};
	
	
	public CarSelectionScreen(String id) {
		super(id);
		spriteBatch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("car-showroom.jpg"));
		arrowLeft = new Texture(Gdx.files.internal("arrow-left.jpg"));
		arrowRight = new Texture(Gdx.files.internal("arrow-right.jpg"));
		arrowUp = new Texture(Gdx.files.internal("arrow-up.jpg"));
		arrowDown = new Texture(Gdx.files.internal("arrow-down.jpg"));

		viewMatrix = new Matrix4();
		tranMatrix = new Matrix4();
		currentSprite = 0;

		font = new BitmapFont(Gdx.files.internal("fonts/imprint50White.fnt"));
		fontImprint = new BitmapFont(Gdx.files.internal("fonts/imprint70Red.fnt"));
		fontImprintWhite = new BitmapFont(Gdx.files.internal("fonts/imprint60White.fnt"));

		progress = 0;
		music = Gdx.audio.newMusic(Gdx.files.internal("racing_song.mp3"));
		music.setLooping(true);
		music.setVolume(0.5f);
		music.play();
		selectedCar = 0;
		numberLaps = 1;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		spriteBatch.dispose();
		texture.dispose();
		music.dispose();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		if (loaded) {
			//if (Gdx.input.justTouched()) {
			if (Commands.comandos[Commands.GO]) {
				Commands.comandos[Commands.GO] = false;
				Commands.valores[Commands.GO] = 0;
				music.stop();
				setDone(true);
			}
		}
		
		if (selectedCar > 0 && Commands.comandos[Commands.SETAESQUERDA] == true) {
			selectedCar--;
			System.out.println(carOptions[selectedCar]);
			
		}
		if (selectedCar < carOptions.length-1 && Commands.comandos[Commands.SETADIREITA] == true) {
			selectedCar++;
			System.out.println(carOptions[selectedCar]);
		}
		
		
		if (numberLaps > 1 && Commands.comandos[Commands.SETABAIXO] == true) {
			numberLaps--;
			System.out.println(numberLaps);
			
		}
		if  (numberLaps < 10 && Commands.comandos[Commands.SETACIMA] == true) {
			numberLaps++;
			System.out.println(numberLaps);
		}
		

		Commands.comandos[Commands.SETAESQUERDA] = false;
		Commands.valores[Commands.SETAESQUERDA] = 0;
		Commands.comandos[Commands.SETADIREITA] = false;
		Commands.valores[Commands.SETADIREITA] = 0;
		Commands.comandos[Commands.SETABAIXO] = false;
		Commands.valores[Commands.SETABAIXO] = 0;
		Commands.comandos[Commands.SETACIMA] = false;
		Commands.valores[Commands.SETACIMA] = 0;
		
		progress = (int)(RacingGame.assetManager.getProgress()*100);
		RacingGame.assetManager.update();
		if (progress == 100) {
			loaded = true;
		}
		time += delta;
		if (time >= 0.5f) {
			visible = !visible;
			time=0;
		}
					
	}

	@Override
	public void draw(float delta) {		
		// tornando meu plano cartesiano can�nico 2D
		viewMatrix.setToOrtho2D(0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT);
		// spritebatch, siga esse padr�o de plano cartesiano
		spriteBatch.setProjectionMatrix(viewMatrix);
		// qualquer transforma��o linear (escala, rota��o), ficara armazenada na
		// tranMatrix
		spriteBatch.setTransformMatrix(tranMatrix);
		spriteBatch.begin();
		spriteBatch.draw(texture, 0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT, 0, 0, texture.getWidth(), texture.getHeight(), false, false);
	
		spriteBatch.draw(arrowLeft, 200, 490, 80, 80, 0, 0, 170, 155, false, false);
		spriteBatch.draw(arrowRight, 690, 490, 80, 80, 0, 0, 170, 155, false, false);
		spriteBatch.draw(arrowUp, 1100, 650, 80, 80, 0, 0, 155, 170, false, false);
		spriteBatch.draw(arrowDown, 1100, 340, 80, 80, 0, 0, 155, 175, false, false);
		
		if (visible) {
			font.draw(spriteBatch, ("Press ENTER to start race").toUpperCase(), 350, 150); 
		}
		
		fontImprint.draw(spriteBatch,("Select race options").toUpperCase(), 370, 1100); 	
		
		fontImprintWhite.draw(spriteBatch, "CAR COLOR" , 290, 890); 
		fontImprint.draw(spriteBatch, carOptions[selectedCar].split("_")[0].toUpperCase() , 330, 560); 
	
		fontImprintWhite.draw(spriteBatch, "NUMBER OF LAPS" , 850, 890); 
		fontImprint.draw(spriteBatch, numberLaps == 10 ? "" + numberLaps : "0" + numberLaps , 1105, 550); 
		
		spriteBatch.end();
	}
}
