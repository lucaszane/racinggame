package br.edu.ufabc.games.racing.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

import br.edu.ufabc.games.racing.util.Commands;
import br.edu.ufabc.games.racing.util.Parameters;

public class CreditsScreen extends AbstractScreen{
	private SpriteBatch spriteBatch;
	private Texture     texture;
	private Matrix4 viewMatrix;
	private Matrix4 tranMatrix;
	private BitmapFont fontBigger;
	private BitmapFont fontImprint;

	public CreditsScreen(String id) {
		super(id);
		texture = new Texture(Gdx.files.internal("credits1.jpg"));
		spriteBatch = new SpriteBatch();
		viewMatrix  = new Matrix4();
		tranMatrix  = new Matrix4();
		fontBigger = new BitmapFont(Gdx.files.internal("fonts/imprint-bigger.fnt"));		
		fontImprint = new BitmapFont(Gdx.files.internal("fonts/imprint70Red.fnt"));

		fontBigger.setColor(Color.WHITE);
	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		spriteBatch.dispose();
		texture.dispose();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		if (Gdx.input.justTouched() || Commands.comandos[Commands.GO]) {
			Commands.comandos[Commands.GO] = false;
			Commands.valores[Commands.GO] = 0;
			setDone(true);
		}
	}

	@Override
	public void draw(float delta) {
		// TODO Auto-generated method stub
		viewMatrix.setToOrtho2D(0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT);
		spriteBatch.setProjectionMatrix(viewMatrix);
		spriteBatch.setTransformMatrix(tranMatrix);
		spriteBatch.begin();
		spriteBatch.draw(texture,0,0,Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT,
                                 0,0,texture.getWidth(), texture.getHeight(),
                                 false, false);
		
		fontImprint.draw(spriteBatch, "MADE BY:", 150, 1100);
		fontBigger.draw(spriteBatch, "GABRIEL DURYNEK", 460, 350);
		fontBigger.draw(spriteBatch, "LUCAS ZANE", 560, 250);
		fontBigger.draw(spriteBatch, "NATHALIA TESCAROLLO", 390, 150);
		spriteBatch.end();
	}

}
