package br.edu.ufabc.games.racing.screen;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

import br.edu.ufabc.games.racing.model.GameObject;
import br.edu.ufabc.games.racing.model.ModelFactory;
import br.edu.ufabc.games.racing.model.Car;
import br.edu.ufabc.games.racing.model.Pedra;
import br.edu.ufabc.games.racing.util.ChasingCamera;
import br.edu.ufabc.games.racing.util.Commands;
import br.edu.ufabc.games.racing.util.Parameters;

public class GameScreen extends AbstractScreen {

	private ChasingCamera camera;
	private Environment environment;
	private ModelBatch modelBatch;
	private Model modeloOriginal;
	private CameraInputController inputController;
	private GameObject chao;
	private GameObject begin;
	private Car car;
	private SpriteBatch spriteBatch;
	private Matrix4 viewMatrix;
	private Matrix4 tranMatrix;
	private BitmapFont font;
	private BitmapFont fontBigger;
	private boolean finish;
	private int laps;
	private int numberLaps;
	private long startTime;
	private long startLap;
	private long totalTime;
	private long lapTime;
	private long bestTime;
	public ArrayList<Node> pneus;
	public ArrayList<BoundingBox> pneusBBox;
	public BoundingBox start;
	public boolean lastColided = false;
	private Music music;
	
	public GameScreen(String id) {
		super(id);
		car = new Car(CarSelectionScreen.carOptions[CarSelectionScreen.selectedCar]);
		modelBatch = new ModelBatch();
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
//		camera = new ChasingCamera(67.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), -0.1f, -90);
		camera = new ChasingCamera(67.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), -0.1f, -12);
		camera.setObjectToFollow(car.getCurrent());
		camera.setOffsetYOut(4f);
		camera.setOffsetYIn(2.5f);
		camera.near = 0.1f;
		camera.far = 500f;
		laps = -1;
		numberLaps = CarSelectionScreen.numberLaps;
		
		chao = new GameObject(ModelFactory.getModelbyName("CHAO"));
		chao.transform.translate(0, -10.0f, 0);
		begin = new GameObject(ModelFactory.getModelbyName("track"));
		pneus = new ArrayList<Node>();
		pneusBBox = new ArrayList<BoundingBox>();
		for(Node n: chao.nodes) {
			Vector3 pos = new Vector3();
			BoundingBox teste = new BoundingBox();
			if(n.id.contains("Pneu")) {
				pneus.add(n);
				n.calculateBoundingBox(teste);
				pneusBBox.add(teste);
			}
			else if(n.id.contains("lab_marker")) {
				n.calculateBoundingBox(teste);
				start = teste;
			}
		}
		
		spriteBatch = new SpriteBatch();
		viewMatrix = new Matrix4();
		tranMatrix = new Matrix4();
		font = new BitmapFont(Gdx.files.internal("fonts/imprint.fnt"));
		fontBigger = new BitmapFont(Gdx.files.internal("fonts/imprint-bigger.fnt"));

		startTime = System.currentTimeMillis();
		startLap = startTime;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		car.dispose();
		modelBatch.dispose();
		if(music!=null)
			music.dispose();
	}

	@Override
	public void update(float delta) {
		if (Commands.comandos[Commands.GO]) {
			Commands.comandos[Commands.GO] = false;
			Commands.valores[Commands.GO] = 0;
			car.morrer();
			if(music != null && music.isPlaying()) {
				music.stop();
			}
			setDone(true);
			dispose();
		}
		
		if(finish)
			return;

		if(Commands.comandos[Commands.FRENTE] == true || Commands.comandos[Commands.TRAS] == true) {
			if (Commands.comandos[Commands.FRENTE] == true) {
				car.andar(delta, Commands.valores[Commands.FRENTE]);
				camera.update();
			}
			if (Commands.comandos[Commands.TRAS] == true) {
				car.parar(delta, Commands.valores[Commands.TRAS]);
				camera.update();
			}
		}
		else {
			car.desacelerar(delta);
			camera.update();
		}
		if(Commands.comandos[Commands.DIREITA] == true || Commands.comandos[Commands.ESQUERDA] == true) {
			if (Commands.comandos[Commands.ESQUERDA] == true) {
				car.virarEsquerda(delta, Commands.valores[Commands.ESQUERDA]);
			}
			if (Commands.comandos[Commands.DIREITA] == true) {
				car.virarDireita(delta, Commands.valores[Commands.DIREITA]);
			}
		}
		else {
			car.pararDeVirar(delta);
		}
		if(Commands.comandos[Commands.CAMERADIREITA] == true || Commands.comandos[Commands.CAMERAESQUERDA] == true) {
			if (Commands.comandos[Commands.CAMERAESQUERDA] == true) {
				car.virarCameraEsquerda(delta, Commands.valores[Commands.CAMERAESQUERDA]);
			}
			if (Commands.comandos[Commands.CAMERADIREITA] == true) {
				car.virarCameraDireita(delta, Commands.valores[Commands.CAMERADIREITA]);
			}
		}

		if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {
			camera.setMode(ChasingCamera.ZOOM_IN);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
			camera.setMode(ChasingCamera.ZOOM_OUT);
		}
		if (car.getEstado() != Car.DEAD)
			car.update(delta);

		checkColisions();
	}

	@Override
	public void draw(float delta) {
		// TODO Auto-generated method stub
		// �rea �til de vis�o que servir� de atributo da camera

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 1);

		modelBatch.begin(camera);

		modelBatch.render(chao);
		Vector3 teste = new Vector3();
		if (car.getEstado() != Car.DEAD) {
			modelBatch.render(car.getCurrent(), environment);
			car.getCurrent().transform.getTranslation(teste);
		}

		modelBatch.end();

		camera.update();

		viewMatrix.setToOrtho2D(0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT);
		spriteBatch.setProjectionMatrix(viewMatrix);
		spriteBatch.setTransformMatrix(tranMatrix);
		spriteBatch.begin();
		
		font.draw(spriteBatch, "LAPS: " + laps + "/" + numberLaps, 20, 1060);
		font.draw(spriteBatch, "SPEED: " + (int)(car.velocidade.z*40) + " km/h", 20, 980);
		font.draw(spriteBatch, "PRESS ENTER TO QUIT", 540, 1150);

		
		if (laps >= numberLaps) {
			fontBigger.draw(spriteBatch, "RACE FINISHED!", 520, 980);
			fontBigger.draw(spriteBatch, "YOU WIN!", 620, 920);
			String trofeu;
			if(bestTime < 45) {
				trofeu = "GOLD";
			}
			else if(bestTime < 65) {
				trofeu = "SILVER";
			}
			else if(bestTime < 85) {
				trofeu = "BRONZE";
			}
			else {
				trofeu = "PARTICIPATION";
			}
			fontBigger.draw(spriteBatch, "YOU WON THIS TROPHY:\n                 " + trofeu, 420, 820);
			
			car.morrer();
			if(!isDone() && (music == null || !music.isPlaying())) {
				music = Gdx.audio.newMusic(Gdx.files.internal("winning_song.mp3"));
				music.setLooping(true);
				music.play();
			}
		}
		else if (finish)
			fontBigger.draw(spriteBatch, "YOU LOSE", 610, 870);
		else {
			totalTime = (System.currentTimeMillis() - startTime)/(long)1000.0;

			lapTime = (System.currentTimeMillis() - startLap)/(long) 1000.0;
			
			if(laps == 0)
				bestTime = totalTime;
		}

		long min = totalTime/60;
		long sec = totalTime%60;
		font.draw(spriteBatch, "Time: " + min + "' " + sec + "'' ", 20, 900);
		
		min = lapTime/60;
		sec = lapTime%60;
		font.draw(spriteBatch, "Lap Time: " + min + "' " + sec + "'' ", 20, 820);
		
		min = bestTime/60;
		sec = bestTime%60;
		font.draw(spriteBatch, "Best Lap: " + min + "' " + sec + "'' ", 20, 740);
		spriteBatch.end();

	}

	public void checkColisions() {
			BoundingBox teste = new BoundingBox();
			
			for(BoundingBox n: pneusBBox) {
				if(car.getCurrent().collidesWith(n)) {
					finish = true;
					if(!isDone() && (music == null || !music.isPlaying())) {
						music = Gdx.audio.newMusic(Gdx.files.internal("losing_song.mp3"));
						music.setLooping(true);
						music.play();
					}
					car.morrer();
					break;
				} 
				if (start != null && car.getCurrent().collidesWith(start)) {
					if(!lastColided) {
						lapTime = (System.currentTimeMillis() - startLap)/(long) 1000.0;
						if(lapTime < bestTime) {
							bestTime = lapTime;
						}
						
						startLap = System.currentTimeMillis();
						laps++;
						lastColided = true;
					}
				}
				else {
					lastColided = false;
				}
			}
	}

}
