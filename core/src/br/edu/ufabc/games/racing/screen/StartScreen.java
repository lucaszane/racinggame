package br.edu.ufabc.games.racing.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

import br.edu.ufabc.games.racing.RacingGame;
import br.edu.ufabc.games.racing.util.Commands;
import br.edu.ufabc.games.racing.util.Parameters;

public class StartScreen extends AbstractScreen {
	private Texture texture;
	private int currentSprite;
	private final int SPRITE_WIDTH = 91;
	private final int SPRITE_HEIGHT = 94;
	private SpriteBatch spriteBatch;
	private Matrix4 viewMatrix;
	private Matrix4 tranMatrix;
	private BitmapFont font;
	private BitmapFont fontImprint;
	private boolean loaded = false;
	private int progress;
	private float time=0;
	private boolean visible=false;
	private Music   music;

	public StartScreen(String id) {
		super(id);
		spriteBatch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("race_start.jpg"));
		viewMatrix = new Matrix4();
		tranMatrix = new Matrix4();
		currentSprite = 0;
		font = new BitmapFont(Gdx.files.internal("fonts/imprint60White.fnt"));
		fontImprint = new BitmapFont(Gdx.files.internal("fonts/imprint70Red.fnt"));
		progress = 0;
		music = Gdx.audio.newMusic(Gdx.files.internal("racing_song.mp3"));
		music.setVolume(0.5f);
		music.setLooping(true);
		music.play();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		spriteBatch.dispose();
		texture.dispose();
		music.dispose();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		if (loaded) {
			if (Gdx.input.justTouched() || Commands.comandos[Commands.GO]) {
				Commands.comandos[Commands.GO] = false;
				Commands.valores[Commands.GO] = 0;
				setDone(true);
				music.stop();
			}
		}

		progress = (int)(RacingGame.assetManager.getProgress()*100);
		RacingGame.assetManager.update();
		if (progress == 100) {
			loaded = true;
		}
		time += delta;
		if (time >= 0.5f) {
			visible = !visible;
			time=0;
		}
				
	}

	@Override
	public void draw(float delta) {
		
		// TODO Auto-generated method stub
		// tornando meu plano cartesiano can�nico 2D
		viewMatrix.setToOrtho2D(0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT);
		// spritebatch, siga esse padr�o de plano cartesiano
		spriteBatch.setProjectionMatrix(viewMatrix);
		// qualquer transforma��o linear (escala, rota��o), ficara armazenada na
		// tranMatrix
		spriteBatch.setTransformMatrix(tranMatrix);
		spriteBatch.begin();
		spriteBatch.draw(texture, 0, 0, Parameters.GAME_WIDTH, Parameters.GAME_HEIGHT, 0, 0, texture.getWidth(), texture.getHeight(), false, false);
		if (!loaded)
		    font.draw(spriteBatch, "Loading..."+progress+"%", 200, 250);
		else {
			if (visible) {
				font.draw(spriteBatch,"Touch to Start!", 200, 250); 
			}
		}
		
		fontImprint.draw(spriteBatch, "THE RACING GAME", 100, 1130);

		spriteBatch.end();
	}

}
