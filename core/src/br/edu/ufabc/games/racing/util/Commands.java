package br.edu.ufabc.games.racing.util;

import com.badlogic.gdx.controllers.Controller;

public class Commands {
    public static boolean[] comandos= {false, false, false, false, false, false, false, false, false, false, false};
    public static float[] valores= {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    public static final int FRENTE   = 0;
    public static final int TRAS     = 1;
    public static final int ESQUERDA = 2;
    public static final int DIREITA  = 3;
    public static final int CAMERAESQUERDA = 4;
    public static final int CAMERADIREITA  = 5;
    public static final int SETADIREITA = 6;
    public static final int SETAESQUERDA = 7;
    public static final int SETACIMA = 8;
    public static final int SETABAIXO = 9;
    public static final int GO = 10;
} 
